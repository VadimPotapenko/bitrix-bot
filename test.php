<?php
include_once ('config.php');
$host = 'http://dev3.nicedo.ru/vadim/bot.4.0/work_zone.php';
$auth = array(
	'domain' => $appsConfig['eb0380425cf67f95955da755ee063c9c']['AUTH']['domain'],
	'member_id'=> $appsConfig['eb0380425cf67f95955da755ee063c9c']['AUTH']['member_id'], 
	'application_token' => $appsConfig['eb0380425cf67f95955da755ee063c9c']['AUTH']['application_token'],
	'refresh_token' => $appsConfig['eb0380425cf67f95955da755ee063c9c']['AUTH']['refresh_token']
);

$data = array('auth' => $auth, 'event' => 'TESTEVENT');

$auth_res = restAuth($auth);
// $result = call ($host, $data);
writeToLog($auth_res, 'Аутентификация');
echo '<pre>';
print_r ($auth_res);
echo '</pre>';


function call ($host, $data) {
	$result = file_get_contents($host.'?'.http_build_query($data));
	return json_decode($result, 1);
}


function restAuth ($auth) {
	if (!isset($auth['refresh_token'])) return 'Do not work';

	$queryUrl = 'https://oauth.bitrix.info/oauth/token';
	$queryData = http_build_query(array(
		'grant_type' => 'refresh_token',
		'refresh_token' => $auth['refresh_token']
	));

	$curl = curl_init($curl);
	curl_setopt_array($curl, array(
		CURLOPT_HEADER => 0,
		CURLOPT_RETURNTRANSFER => 1,
		CURLOPT_URL => $queryUrl.'?'.$queryData
	));

	$result = curl_exec($curl);
	curl_close($curl);
	$result = json_decode($result, 1);
	return $result;

	// if (!isset($resutl['error'])) {
		// $appsConfig = array();

		// if (file_exists(__DIR__.'/config.php')) include (__DIR__.'/config.php');
		// $result['application_token'] = $auth['application_token'];
		// $appsConfig[$auth['application_token']['AUTH']] = $result;
		// $this->saveParams($appsConfig);
	// } else { return 'Do not work'; }
}

function writeToLog ($data, $title='DEBUG') {
	$log = "\n--------------------\n";
	$log .= date('H:i:s')."\n";
	$log .= $title."\n";
	$log .= print_r($data, 1);
	$log .= "\n--------------------\n";

	file_put_contents('testDebug.txt', $log);
	return true;
}