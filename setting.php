<?php
return array (
	'botParams' => array(
		'CODE'       => 'B_4.0',
		'TYPE'       => 'B',
		'OPENLINE'   => 'N',
		'PROPERTIES' => array(
			'NAME'              => 'Бот Василий',
			'COLOR'             => 'GREEN',
			'EMAIL'             => 'test@local.dev',
			'PERSONAL_BIRTHDAY' => '2019-10-15',
			'WORK_POSITION'     => 'Бот',
			'PERSONAL_WWW'      => 'https://nicedo.ru',
			'PERSONAL_GENDER'   => 'm',
			'PERSONAL_PHOTO'    => base64_encode(file_get_contents(__DIR__.'/avatar.png'))
		)
	),
	'keyboards' => array(
		// 'GitHub' => array(
		// 	'TEXT'       => 'GitHub',
		// 	'LINK'       => 'https://github.com/',
		// 	'BG_COLOR'   => '#000',
		// 	'TEXT_COLOR' => '#fff',
		// 	'DISPLAY'    => 'LINE'
		// ),
		// 'Google' => array (
		// 	'TEXT'       => 'Google',
		// 	'LINK'       => 'https://google.com',
		// 	'BG_COLOR'   => '#f00',
		// 	'TEXT_COLOR' => '#fff',
		// 	'DISPLAY'    => 'LINE'
		// )
	),
	'commands' => array(
		'deals' => array(
			'COMMAND' => 'deals',
			'COMMON'  => 'N',
			'HIDDEN'  => 'N',
			'LANG'    => array(
				array(
					'LANGUAGE_ID' => 'en',
					'TITLE'       => 'Show my deals',
					'PARAMS'      => 'Command show your deals'
				)
			)
		),
		'helpme' => array(
			'COMMAND' => 'helpme',
			'COMMON'  => 'N',
			'HIDDEN'  => 'N',
			'LANG'    => array(
				array(
					'LANGUAGE_ID' => 'en',
					'TITLE'       => 'Instrution',
					'PARAMS'      => 'Show instrution for command deals'
				)
			)
		),
		// 'links' => array(
		// 	'COMMAND' => 'links',
		// 	'COMMON'  => 'Y',
		// 	'HIDDEN'  => 'N',
		// 	'LANG'    => array(
		// 		array(
		// 			'LANGUAGE_ID' => 'en',
		// 			'TITLE'       => 'Most popular links',
		// 			'PARAMS'      => 'This command show most popular links'
		// 		)
		// 	)
		// )
	)
);