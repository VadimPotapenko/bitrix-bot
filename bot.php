<?php
class Bot {
	public $bot_info;

	public function __construct () {
		$file = __DIR__.'/setting.php';
		if (file_exists($file)) {
			$info = include ($file);
			$this->bot_info = $info;
		}
	}

	public function getEvent ($event) {
		switch ($event) {
			case 'ONAPPINSTALL':
			#### Регистрация бота ####
				$handlerUrl = (isset($_SERVER['HTTPS']) ? 'https:' : 'http:').'//'.$_SERVER['SERVER_NAME'].$_SERVER['SCRIPT_NAME'];
				$params = array_merge($this->bot_info['botParams'], array(
					'EVENT_MESSAGE_ADD' => $handlerUrl,
					'EVENT_WELCOME_MESSAGE' => $handlerUrl,
					'EVENT_BOT_DELETE' => $handlerUrl
				));

				$result = $this->restCommand('imbot.register', $params, $_REQUEST['auth']);
				$result = json_decode($result, 1);
				$botId = $result['result'];

				$result = $this->restCommand('event.bind', array(
					'EVENT' => 'OnAppUpdate',
					'HANDLER' => $handlerUrl
				), $_REQUEST['auth']);

				$appsConfig[$_REQUEST['auth']['application_token']] = array (
					'BOT_ID' => $botId,
					'AUTH' => $_REQUEST['auth']
				);
				$this->saveParams($appsConfig);
				$this->writeToLog('Новый бот id: '.$botId, 'Зарегистрован чат-бот');

			#### Регистрация комманд бота ####
				foreach ($this->bot_info['commands'] as $command) {
					$commandArr = array_merge($command, array('BOT_ID' => $botId, 'EVENT_COMMAND_ADD' => $handlerUrl));
					$new_command = $this->restCommand('imbot.command.register', $commandArr, $_REQUEST['auth']);
					$this->writeToLog(json_decode($new_command, 1), 'Зарегистрированна новая команда');
				}

			#### Пробуем приветственное сообщение от бота ####
				$message = 'Я родился!';
				$sayHell = $this->restCommand('imbot.message.add', array(
					'BOT_ID' => $botId,
					'DIALOG_ID' => '1',
					'MESSAGE' => $message
				), $_REQUEST['auth']);

				break;

			case 'ONIMBOTJOINCHAT':

				$this->writeToLog($_REQUEST, 'Бот приглашает в чат');

				$message = 'Привет! Я, бот Василий! Я помогу вам контролировать и анализировать ваши сделки.';
				$botJoinInChat = $this->restCommand('imbot.message.add', array(
					'BOT_ID' => $_REQUEST['data']['PARAMS']['BOT_ID'],
					'DIALOG_ID' => $_REQUEST['data']['PARAMS']['DIALOG_ID'],
					'MESSAGE' => $message
				), $_REQUEST['auth']);

				$this->writeToLog($botJoinInChat, 'Ответ на приглашение в чат');

				break;

				case 'ONIMBOTMESSAGEADD':
					// code...

					break;

				case 'ONIMCOMMANDADD':
					// code...

					break;

		}
		return true;
	}

	### :) ###
	public function restCommand ($method, $params = array(), $auth = array(), $refreshToken = true) {
		$queryUrl = $auth['client_endpoint'].$method;
		$queryData = http_build_query(array_merge($params, array('auth' => $auth['access_token'])));

		$curl = curl_init();
		curl_setopt_array($curl, array(
			CURLOPT_POST => 1,
			CURLOPT_HEADER => 0,
			CURLOPT_RETURNTRANSFER => 1,
			CURLOPT_SSL_VERIFYPEER => 1,
			CURLOPT_URL => $queryUrl,
			CURLOPT_POSTFIELDS => $queryData
		));

		$result = curl_exec($curl);
		curl_close($curl);

		if ($refreshToken && isset($result['error']) && in_array($result['error'], array('expired_token', 'invalid_token'))) {
			$auth = $this->restAuth($auth);
			if ($auth) $result = $this->restCommand($method, $params, $auth, false);
		}
		$res = json_decode($result, 1);
		return $res;
	}

	public function restAuth ($auth) {
		if (!isset($auth['refresh_token'])) return false;

		$queryUrl = 'https://oauth.bitrix.info/oauth/token';
		$queryData = http_build_query(array(
			'grant_type' => 'refresh_token',
			'refresh_token' => $auth['refresh_token']
		));

		$curl = curl_init($curl);
		curl_setopt_array($curl, array(
			CURLOPT_HEADER => 0,
			CURLOPT_RETURNTRANSFER => 1,
			CURLOPT_URL => $queryUrl.'?'.$queryData
		));

		$result = curl_exec($curl);
		curl_close($curl);
		$result = json_decode($result, 1);

		if (!isset($resutl['error'])) {
			$appsConfig = array();

			if (file_exists(__DIR__.'/config.php')) include (__DIR__.'/config.php');
			$result['application_token'] = $auth['application_token'];
			$appsConfig[$auth['application_token']['AUTH']] = $result;
			$this->saveParams($appsConfig);
		} else { return false; }
	}

	public function saveParams ($params) {
		$config = "<?php\n";
		$config .= "\$appsConfig = ".var_export($params, true)."\n";
		$config .= "?>";

		file_put_contents ('config.php', $config);
		return true;
	}

	public function writeToLog ($data, $title = 'DEBUG') {
		$log = "\n--------------------\n";
		$log .= date("d.m.Y H:i:s")."\n";
		$log .= $title."\n";
		$log .= print_r($data, 1);
		$log .= "\n--------------------\n";

		file_put_contents(__DIR__.'/debug.txt', $log, FILE_APPEND);
		return true;
	}

	/* test */
	public function batch ($data) {
		if (is_array($data)) {
			foreach ($data as $k => $v) {
				$dataRest['cmd'][$k] = $v['method'];
				$dataRest['cmd'][$k] .= '?'.http_build_query($v['params']);
			}
		}
		return $dataRest;
	}
}