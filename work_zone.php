<?php
#======================= setting ==========================#
if (file_exists('config.php')) include_once ('config.php');# ?
include_once ('bot.php');
$bot = new Bot;

$lib = array (
	'!activity',
	'ID',
	'TITLE',
	'DATE_CREATE_FROM',
	'DATE_CREATE_TO',
	'DATE_CREATE'
);
#==========================================================#

### bot run ###
// $result = $bot->getEvent($_REQUEST['event']);
$result = $bot->restAuth($appsConfig['eb0380425cf67f95955da755ee063c9c']['AUTH']);
$bot->writeToLog($result, 'AUTH');
echo '<pre>';
print_r ($result);
echo '</pre>';

### bitrix event ###
switch ($_REQUEST['event']) {
	case 'ONAPPINSTALL':
		$result = $bot->getEvent($_REQUEST['event']);
		break;
	
	case 'ONIMBOTJOINCHAT':
		$result = $bot->getEvent($_REQUEST['event']);
		break;

	case 'ONIMBOTMESSAGEADD':
		$answer = $bot->restCommand('imbot.message.add', array(
			'BOT_ID' => $_REQUEST['data']['PARAMS']['BOT_ID'],
			'DIALOG_ID' => $_REQUEST['data']['PARAMS']['DIALOG_ID'],
			'MESSAGE' => 'Простите, нам не следует общаться.'
		), $_REQUEST['auth']);
		break;

	case 'ONIMCOMMANDADD':
		foreach ($_REQUEST['data']['COMMAND'] as $command) {
			$bot->writeToLog($_REQUEST, 'Боту поступила команда');
			### command deals ###
			if ($command['COMMAND'] == 'deals') {
				if (strlen($command['COMMAND_PARAMS']) > 0) {
					$bot->writeToLog($command['COMMAND_PARAMS'], 'Боту поступил фильтр');
					### ветка с установленным пользователем фильтром ###
					### сделки без дела ###
					if ($command['COMMAND_PARAMS'] == '!activity') {
						$bot->writeToLog($command['COMMAND_PARAMS'], 'Бот распознал фильтр !activity');

						// сделки без дела...
						### получаем дела ###
						$filterActivity = array(
							'filter' => array('OWNER_TYPE_ID' => '2', 'STATUS' => '1'),
							'select' => array('ID', 'OWNER_ID', 'DEADLINE', 'RESPONSIBLE_ID')
						);

						$activityTotal = $bot->restCommand('crm.activity.list', $filterActivity, $_REQUEST['auth']);
						$iteration = intval($activityTotal['total'] / 50) + 1;
						if ($activityTotal['total'] % 50 == 0) $iteration -= 1;
						for ($i = 0; $i < $iteration; $i++) {
							$start = $i * 50;
							$activityData[] = array(
								'method' => 'crm.activity.list',
								'params' => array(
									'filter' => $filterActivity['filter'],
									'select' => $filterActivity['select'],
									'start' => $start
								)
							);
						}

						if (count($activityData) > 50) $activityDataRes = array_chunk($activityData, 50);
						else $activityDataRes = array($activityData);

						for ($i = 0, $s = count($activityDataRes);$i < $s; $i++) {
							$activityBatch = $bot->batch($activityDataRes[$i]);
							$activity[] = $bot->restCommand('batch', $activityBatch, $_REQUEST['auth']);
						}
						$bot->writeToLog($activity, 'Получаем дела');

						### получаем сделки ###
						$filterDeals = array(
							'filter' => array('CLOSED' => 'N'),
							'select' => array('ID', 'ASSIGNED_BY_ID', 'TITLE')
						);

						$dealsTotal = $bot->restCommand('crm.deal.list', $filterDeals, $_REQUEST['auth']);
						//$bot->writeToLog($dealsTotal, 'Total сделок');

						$iteration = intval($dealsTotal['total'] / 50) + 1;
						if ($dealsTotal['total'] % 50 == 0) $iteration -= 1;
						for ($i = 0; $i < $iteration; $i++) {
							$start = $i * 50;
							$dealsData[] = array(
								'method' => 'crm.deal.list',
								'params' => array(
									'filter' => $filterDeals['filter'],
									'select' => $filterDeals['select'],
									'start' => $start
								)
							);
						}

						if (count($dealsData) > 50) $dealsDataRes = array_chunk($dealsData, 50);
						else $dealsDataRes = array($dealsData);
						//$bot->writeToLog($dealsDataRes, 'Массив для запроса сделок клиента');

						for ($i = 0, $s = count($dealsDataRes); $i < $s; $i++) {
							$dealsBatch = $bot->batch($dealsDataRes[$i]);
							$deals[] = $bot->restCommand('batch', $dealsBatch, $_REQUEST['auth']);
						}
						$bot->writeToLog($deals, 'Получаем сделки');

						### анализ сделок и дел ###
						### ищем сделки без дел ###
						### к ответственному привязываем все сделки ###
						foreach ($deals as $value) {
							foreach ($value['result']['result'] as $v){
								for ($i = 0, $s = count($v); $i < $s; $i++) {
									$dealsArr[$v[$i]['ASSIGNED_BY_ID']][$i]['id'] = $v[$i]['ID'];
									$dealsArr[$v[$i]['ASSIGNED_BY_ID']][$i]['title'] = $v[$i]['TITLE'];
								}
							}
						}
						$backUpDeals = $dealsArr;
						$bot->writeToLog($backUpDeals, 'backUpDeals');

						### к ответственному привязываем все дела ###
						foreach ($activity as $active) {
							foreach ($active['result']['result'] as $value) {
								for ($i = 0, $s = count($value); $i < $s; $i++) {
									$activArr[$value[$i]['RESPONSIBLE_ID']][$i] = $value[$i]['OWNER_ID'];
								}
							}
						}
						//$bot->writeToLog($activArr, 'activArr');

						### ищем сделки без дел ###
						foreach ($dealsArr as $key => $value) {
							for ($i = 0, $s = count($value); $i < $s; $i++){
								foreach ($activArr as $k => $v) {
									for ($y = 0, $size = count($v); $y < $size; $y++) {
										if ($key == $k && $value[$i]['id'] == $v[$y]) {
											unset($dealsArr[$key][$i]);
										}
									}
								}
							}
						}
						//$bot->writeToLog($dealsArr, 'workArr1');

						foreach ($dealsArr as $k => $v) {
							for ($i = 0, $s = count($v); $i < $s; $i++){
								if (empty($v[$i]['id'])) unset($dealsArr[$k][$i]);
							}
						}
						//$bot->writeToLog($dealsArr, 'workArr2');

						### сообщение о сделках без дел ###
						$messageOne = "У вас есть сделки с не оформленными делами:\n";
						foreach ($dealsArr as $k => $v) {
							for ($i = 0, $s = count($v); $i < $s; $i ++) {
								$messageOne .= "[URL=".$command['domain'].'/crm/deal/details/'.$v[$i]['id'].'/]'.$v[$i]['title']."[/URL];\n";
							}
						}
						//$bot->writeToLog($messageOne, 'подготовленное сообщение для бота');

						$dealsAnswer = $bot->restCommand('imbot.message.add', array(
							'BOT_ID' => $_REQUEST['data']['PARAMS']['BOT_ID'],
							'DIALOG_ID' => $_REQUEST['data']['PARAMS']['DIALOG_ID'],
							'MESSAGE' => $messageOne
						), $_REQUEST['auth']);
						$bot->writeToLog($dealsAnswer, 'ответ бота сделки без дел');

						### ищем сделки с просроченными делами ###
						foreach ($activity as $active) {
							foreach ($active['result']['result'] as $act) {
								foreach ($act as $a) {
									$d = explode('T', $a['DEADLINE'])[0];
									$delay = getDelay($d);
									if ($delay > 0) {
										$delArr[$a['RESPONSIBLE_ID']][] = $a['OWNER_ID'];
									}
								}
							}
						}
						$bot->writeToLog($delArr, 'delArr');

						### ###
						foreach ($delArr as $key => $value) {
							for ($y = 0, $size = count($value); $y < $size; $y++) {
								foreach ($backUpDeals as $k => $v) {
									for ($i = 0, $s = count($v); $i < $s; $i++) {
										if ($key == $k && $value[$y] == $v[$i]['id']) {
											$delayArr[$k][$i]['id'] = $v[$i]['id'];
											$delayArr[$k][$i]['title'] = $v[$i]['title'];
										}
									}
								}
							}
						}
						$bot->writeToLog($delayArr, 'delayArr');

						### сообщение о сделках с просроченными делами ###
						$messageTwo = "У вас есть сделки с просроченными делами:\n";
						foreach ($delayArr as $k => $v) {
							for ($i = 0, $s = count($v); $i < $s; $i++) {
								$messageTwo .= "[URL=".$command['domain'].'/crm/deal/details/'.$v[$i]['id'].'/]'.$v[$i]['title']."[/URL];\n";
							}
						}
						$delayAnswer = $bot->restCommand('imbot.message.add', array(
							'BOT_ID' => $_REQUEST['data']['PARAMS']['BOT_ID'],
							'DIALOG_ID' => $_REQUEST['data']['PARAMS']['DIALOG_ID'],
							'MESSAGE' => $messageTwo
						), $_REQUEST['auth']);
						$bot->writeToLog($delayAnswer, 'ответ бота сделки с просреченными делами');

					} else {
						$valid = preg_match('~:~', $command['COMMAND_PARAMS']);
						$bot->writeToLog($valid, 'Проверка на наличие : в структуре фильтра');
						if ($valid) {
							$tmp = explode(':', $command['COMMAND_PARAMS']);
							$bot->writeToLog($tmp, 'Разбор фильтра');
							switch (strtoupper($tmp[0])) {

								### фильтр по id ###
								case 'ID':
									$filter = array('ID' => $tmp[1]);
									break;
								
								### фильтр по названию ###
								case 'TITLE':
									$filter = array('TITLE' => $tmp[1]);
									break;

								### фильтр по дате создания ###
								case 'DATE_CREATE':
									$filter = array('DATE_CREATE' => $tmp[1]);
									break;

								### фильтр >date_create ###
								case 'DATE_CREATE_TO':
									$filter = array('< DATE_CREATE' => $tmp[1]);
									break;

								case 'DATE_CREATE_FROM':
									$filter = array('> DATE_CREATE' => $tmp[1]);
									break;

								### фильтр отсутствует в библиотеке бота ###
								default:
									$debug = $bot->restCommand('imbot.message.add', array(
										'BOT_ID' => $_REQUEST['data']['PARAMS']['BOT_ID'],
										'DIALOG_ID' => $_REQUEST['data']['PARAMS']['DIALOG_ID'],
										'MESSAGE' => 'Прошу прощения, комманда не найдена в моем справочнике. Я сообщу разработчикам! хеххе шучу, не сообщу!'
									), $_REQUEST['auth']);
									die();
							}
							$bot->writeToLog($filter, 'Найден фильтр');

							### формируем массив дл батч запроса ###
							$dealTotal = $bot->restCommand('crm.deal.list', array(
								'filter' => $filter,
								'select' => array('ID'),
							), $_REQUEST['auth']);

							$iteration = intval($dealTotal['total'] / 50) + 1;
							if ($dealTotal['total'] % 50 == 0) $iteration -= 1;

							for ($i = 0; $i < $iteration; $i++) {
								$start = $i * 50;
								$dealsData[] = array(
									'method' => 'crm.deal.list',
									'params' => array(
										'filter' => $filter,
										'select' => array('ID', 'TITLE'),
										'start' => $start
									)
								);
							}

							if (count($dealsData) > 50) $dealsDataRes = array_chunk($dealsData, 50);
							else $dealsDataRes = array($dealsData);
							$bot->writeToLog($dealsDataRes, 'Массив для запроса сделок');

							### реализуем батч запрос на получение сделок ###
							for ($i = 0, $s = count($dealsDataRes); $i < $s; $i++) {
								$dealsBatch = $bot->batch($dealsDataRes[$i]);
								$resultDeals[] = $bot->restCommand('batch', $dealsBatch, $_REQUEST['auth']);
							}
							$bot->writeToLog($resultDeals, 'Полученные сделки');

							### ответное сообщение со списком сделок ###
							$message = 'Ваши сделки полученные по фильтру '.$command['COMMAND_PARAMS'].":\n";
							foreach ($resultDeals as $packet) {
								foreach ($packet['result']['result'] as $value) {
									foreach ($value as $v) {
										$message .= "[URL=".$command['domain'].'/crm/deal/details/'.$v['ID'].'/]'.$v['TITLE']."[/URL];\n";
									}
								}
							}

							$answerDeals = $bot->restCommand('imbot.message.add', array(
								'BOT_ID' => $_REQUEST['data']['PARAMS']['BOT_ID'],
								'DIALOG_ID' => $_REQUEST['data']['PARAMS']['DIALOG_ID'],
								'MESSAGE' => $message
							), $_REQUEST['auth']);
							$bot->writeToLog($answerDeals, 'Бот предоставил сделки юзеру');

						} else {
							### не верный синтаксис фильтра (отсутствует :) ###
							$answer = $bot->restCommand('imbot.message.add', array(
								'BOT_ID' => $_REQUEST['data']['PARAMS']['BOT_ID'],
								'DIALOG_ID' => $_REQUEST['data']['PARAMS']['DIALOG_ID'],
								'MESSAGE' => 'Извините, к сожалению параметры к команде /deals, введены не верно. Правильный синтаксис команды: [B]/deals ID:XX[/B]'
							), $_REQUEST['auth']);
							$bot->writeToLog($answer, 'Бот не распознал структуру команды');
						}
					}
				} else {
					### ветка если пользователь не установил фильтр ###
					$dealsTotal = $bot->restCommand('crm.deal.list', array(
						'select' => array('ID')
					), $_REQUEST['auth']);
					$bot->writeToLog($dealsTotal, 'Total сделок без фильтра');

					$iteration = intval($dealsTotal['total'] / 50) + 1;
					if ($dealsTotal['total'] % 50 == 0) $iteration -= 1;
					$bot->writeToLog($iteration, 'Количество пакетов для массива сделок без фильтра');

					for ($i = 0; $i < $iteration; $i++) {
						$start = $i * 50;
						$dealsData[] = array(
							'method' => 'crm.deal.list',
							'params' => array(
								'select' => array('ID', 'TITLE'),
								'start' => $start
							)
						);
					}

					if (count($dealsData) > 50) $dealsDataRes = array_chunk($dealsData, 50);
					else $dealsDataRes[] = $dealsData;
					$bot->writeToLog($dealsDataRes, 'массив для сделок без фильтра');

					### батч запрос для получения сделок ###
					for ($i = 0, $s = count($dealsDataRes); $i < $s; $i++) {
						$data = $bot->batch($dealsDataRes[$i]);
						$result[] = $bot->restCommand('batch', $data, $_REQUEST['auth']);
					}
					$bot->writeToLog($result, 'сделки без фильтра');

					$message = "Ваши сделки без фильтра:\n";
					foreach ($result as $packet) {
						foreach ($packet['result']['result'] as $value) {
							foreach ($value as $v) {
								$message .= "[URL=".$command['domain'].'/crm/deal/details/'.$v['ID'].'/]'.$v['TITLE']."[/URL];\n";
							}
						}
					}

					### ответ бота по ветке список сделок без фильтра ###
					$answer = $bot->restCommand('imbot.message.add', array(
						'BOT_ID' => $_REQUEST['data']['PARAMS']['BOT_ID'],
						'DIALOG_ID' => $_REQUEST['data']['PARAMS']['DIALOG_ID'],
						'MESSAGE' => $message
					), $_REQUEST['auth']);
					$bot->writeToLog($answer, 'ответ бота на запрос сделок без фильтра');

				}
			} elseif ($command['COMMAND'] == 'helpme') {
				### инструкция ###
				$message = "ОК! Сейчас я введу тебя в курс дела! Моя основная задача - это  сбор и анализ твоих сделок. Я могу предоставить ссылки на интересующие тебя сделки отфильтровав их по id, названию, дате создания, а также предоставить сделки без дел.\nЧтобы использовать фильтр необходимо после того, как ты ввел команду [B]/deals[/B] добавить некоторые параметры, смотри вот они:\n[B]ID:XX[/B] - найду сделку по указанному id;\n[B]TITLE:Название[/B] - находим сделку по указаному названию;\n[B]DATE_CREATE:YYYY-MM-DD[/B] - дата создания;\n[B]DATE_CREATE_FROM:YYYY-MM-DD[/B] - сделки от введенной даты создания;\n[B]DATE_CREATE_TO:YYYY-MM-DD[/B] - сделки после введенной даты создания;";
				$answer = $bot->restCommand('imbot.message.add', array(
					'BOT_ID' => $_REQUEST['data']['PARAMS']['BOT_ID'],
					'DIALOG_ID' => $_REQUEST['data']['PARAMS']['DIALOG_ID'],
					'MESSAGE' => $message
				), $_REQUEST['auth']);

				$bot->writeToLog($answer, 'Инструкция от бота');
			} else {
				$message = 'К сожалению эта команда мне не известна';
				$answer = $bot->restCommand('imbot.message.add', array(
					'BOT_ID' => $_REQUEST['data']['PARAMS']['BOT_ID'],
					'DIALOG_ID' => $_REQUEST['data']['PARAMS']['DIALOG_ID'],
					'MESSAGE' => $message
				), $_REQUEST['auth']);
			}
		}

		break;

	case 'TESTEVENT':
		$bot->writeToLog($_REQUEST, 'TestEvent пришел');

		$message = 'Test bot event is work';
		$result = $bot->restCommand( 'imbot.message.add', array(
			'BOT_ID' => '96',
			'DIALOG_ID' =>  '1',
			'MESSAGE' => $message
		),$_REQUEST['auth']);

		$bot->writeToLog($result, 'Бот ответил на TestEvent');
		break;

	default:
		die();
}

########### functions ############
function getDelay ($checkX) {
	$checkInX = explode('-', $checkX);
	$date = mktime(0,0,0, $checkInX[1],$checkInX[2],$checkInX[0]);
	$today  = mktime(0,0,0, date("m"),date("d"),date("Y"));
	$delay = $today - $date;
	return $delay;
}